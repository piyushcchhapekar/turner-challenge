﻿
using System.Collections.Generic;
using Turner.Titles.Model;

namespace Turner.Titles.Service
{
    public interface ITitleService
    {
        IList<Title> SearchTitleByName(string title);
        Title GetTitleById(int id);
    }
}
