﻿
using System.Collections.Generic;
using System.Linq;
using Turner.Titles.Infrastructure.Repository;
using Turner.Titles.Model;

namespace Turner.Titles.Service
{
    
    public class TitleService : ITitleService
    {
        private readonly IRepository<Title> _titleRepository; 
        public TitleService()
        {
            _titleRepository = new BaseRepository<Title>();
        }
        
        public IList<Title> SearchTitleByName(string title)
        {
            return _titleRepository.GetAllSatisfiedBy(x => x.Name.Contains(title)).OrderBy(x => x.Name).ToList();
        }

        public Title GetTitleById(int id)
        {
            return _titleRepository.GetById(id);
        }
    }
}
