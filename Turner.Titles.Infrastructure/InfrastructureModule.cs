﻿using System.Configuration;
using NHibernate;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Turner.Titles.Infrastructure
{
    public class InfrastructureModule : NinjectModule
    {
        public override void Load()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DentalPlans.Admin"].ToString();
            var nHibernateHelper = new NHibernateHelper(connectionString);

            Bind<ISessionFactory>().ToConstant(nHibernateHelper.SessionFactory).InSingletonScope();

            Bind<ISession>().ToProvider(new SessionProvider()).InRequestScope();
        }
    }

    public class SessionProvider : Provider<ISession>
    {
        protected override ISession CreateInstance(IContext context)
        {
            var unitOfWork = (UnitOfWork)context.Kernel.Get<IUnitOfWork>();
            return unitOfWork.Session;
        }
    }
}
