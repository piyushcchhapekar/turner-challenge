﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure.Mapping
{
    public class AwardMap : IAutoMappingOverride<Award>
    {
        public void Override(AutoMapping<Award> awardMapping)
        {
            awardMapping.Map(award => award.Company).Column("AwardCompany");
            awardMapping.Map(award => award.Name).Column("Award");
            awardMapping.Map(award => award.Year).Column("AwardYear");
            awardMapping.Map(award => award.HasWon).Column("AwardWon");
        }
    }
}
