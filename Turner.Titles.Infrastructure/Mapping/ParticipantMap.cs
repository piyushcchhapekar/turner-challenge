﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure.Mapping
{
    public class ParticipantMap : IAutoMappingOverride<Participant>
    {
        public void Override(AutoMapping<Participant> participantMapping)
        {
            participantMapping.Map(participant => participant.Type).Column("ParticipantType");
        }
    }
}
