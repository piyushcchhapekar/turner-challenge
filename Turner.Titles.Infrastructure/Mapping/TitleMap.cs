﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure.Mapping
{
    public class TitleMap : IAutoMappingOverride<Title>
    {
        public void Override(AutoMapping<Title> titleMapping)
        {
            titleMapping.Id(title => title.Id).Column("TitleId");
            titleMapping.Map(title => title.Name).Column("TitleName");

            titleMapping.HasMany(title => title.StoryLines).KeyColumn("TitleId").LazyLoad();
            titleMapping.HasManyToMany(title => title.Genres)
                .Table("TitleGenre")
                .ParentKeyColumn("TitleId")
                .ChildKeyColumn("GenreId");

            titleMapping.HasManyToMany(title => title.Participants)
                .Table("TitleParticipant")
                .ParentKeyColumn("TitleId")
                .ChildKeyColumn("ParticipantId");

            titleMapping.HasMany(title => title.Awards).KeyColumn("TitleId").Where("AwardWon=1");
        }
    }
}
