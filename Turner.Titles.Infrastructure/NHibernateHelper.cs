﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Dialect;
using Turner.Titles.Infrastructure.Mapping;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure
{
    public class NHibernateHelper
    {
        private ISessionFactory _sessionFactory;
        private readonly string _connectionString;
        TurnerConfiguration _configuration;

        public NHibernateHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ISessionFactory SessionFactory
        {
            get
            {
                _configuration = new TurnerConfiguration();
                return _sessionFactory ??
                    (
                        _sessionFactory = Fluently.Configure()
                        .Database(MsSqlConfiguration.MsSql2008.
                                    ConnectionString(_connectionString)
                                    .Dialect<MsSqlAzure2008Dialect>()
                    )
                        .Mappings(x => x.AutoMappings.Add(
                                    AutoMap.AssemblyOf<Title>(_configuration)
                                    .Conventions.Add(DynamicUpdate.AlwaysTrue())
                                    .UseOverridesFromAssemblyOf<TitleMap>))
                        .BuildSessionFactory()
                        );
            }
        }

        public class TurnerConfiguration : DefaultAutomappingConfiguration
        {
            public override bool ShouldMap(Type type)
            {
                return type.Namespace == "Turner.Titles.Model";
            }
        }
    }
}
