﻿
using System;
using System.Configuration;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Dialect;
using Turner.Titles.Infrastructure.Mapping;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure
{
    public class NHibernateSessionFactory
    {
        readonly static string ConnectionString = ConfigurationManager.ConnectionStrings["Turner.Titles"].ToString();
        public static ISession OpenSession()
        {
            var configuration = new TurnerConfiguration();

            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.
                    ConnectionString(ConnectionString)
                    .Dialect<MsSqlAzure2008Dialect>()
                )
                .Mappings(x => x.AutoMappings.Add(
                    AutoMap.AssemblyOf<Title>(configuration)
                        .Conventions.Add(DynamicUpdate.AlwaysTrue())
                        .UseOverridesFromAssemblyOf<TitleMap>))
                .BuildSessionFactory();

            return sessionFactory.OpenSession();
        }

    }

    public class TurnerConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            return type.Namespace == "Turner.Titles.Model";
        }
    }
}
