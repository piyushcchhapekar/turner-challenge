﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly ISession _session;

        public BaseRepository()
        {
            _session = NHibernateSessionFactory.OpenSession();
        }

        public bool Add(T entity)
        {
            _session.Save(entity);
            return true;
        }

        public bool Add(IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                _session.Save(item);
            }
            return true;
        }

        public bool Update(T entity)
        {
            _session.Update(entity);
            return true;
        }

        public bool Delete(T entity)
        {
            _session.Delete(entity);
            return true;
        }

        public bool Delete(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                _session.Delete(entity);
            }
            return true;
        }

        public T GetById(int id)
        {
            return _session.Get<T>(id);
        }

        public IQueryable<T> GetAll()
        {
            return _session.Query<T>();
        }

        public IQueryable<T> GetAllSatisfiedBy(Expression<Func<T, bool>> expression)
        {
            return GetAll().Where(expression).AsQueryable();
        }
    }
}
