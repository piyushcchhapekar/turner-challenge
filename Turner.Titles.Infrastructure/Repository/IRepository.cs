﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Persister.Entity;
using Turner.Titles.Model;

namespace Turner.Titles.Infrastructure.Repository
{
    public interface IRepository<T> where T : IEntity
    {
        bool Add(T entity);
        bool Add(IEnumerable<T> entities);
        bool Update(T entity);
        bool Delete(T entity);
        bool Delete(IEnumerable<T> entities);
        T GetById(int id);
        IQueryable<T> GetAll();
        IQueryable<T> GetAllSatisfiedBy(Expression<Func<T, bool>> expression);

    }
}
