﻿
using Turner.Titles.Model;

namespace Turner.Titles.Model
{
    public class Award : IEntity
    {
        protected Award()
        {
            
        }
        public virtual int Id { get; protected set; }

        public virtual string Name { get; protected set; }
        public virtual string Company { get; protected set; }
        public virtual int Year { get; set; }

        public virtual bool HasWon { get; set; }
    }
}
