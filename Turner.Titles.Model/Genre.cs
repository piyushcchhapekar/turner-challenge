﻿
namespace Turner.Titles.Model
{
    public class Genre
    {
        protected Genre()
        {
            
        }
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
    }
}
