﻿namespace Turner.Titles.Model
{
    public interface IEntity
    {
        int Id { get; }
    }
}
