﻿

namespace Turner.Titles.Model
{
    public class Participant
    {
        protected Participant()
        {
            
        }
        public virtual int Id { get; protected set; }
        public virtual string Name { get; protected set; }
        public virtual string Type { get; protected set; }
    }
}
