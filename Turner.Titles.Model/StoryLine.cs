﻿
namespace Turner.Titles.Model
{
    public class StoryLine : IEntity
    {
        protected StoryLine()
        {
            
        }
        public virtual int Id { get; protected set; }
        public virtual string Language { get; protected set; }
        public virtual string Description { get; protected set; }

        public virtual string Type { get; protected set; }
    }
}
