﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Turner.Titles.Model
{
    public class Title : IEntity
    {
        protected Title()
        {
        }

        public virtual int Id { get; protected set; }
        public virtual int? ReleaseYear { get; protected set; }

        public virtual string Name { get; protected set; }

        public virtual IList<StoryLine> StoryLines { get; protected set; }

        public virtual IList<Genre> Genres { get; protected set; }

        public virtual IList<Participant> Participants { get; protected set; }

        public virtual IList<Award> Awards { get; protected set; } 

    }
}
