﻿using System.Web;
using System.Web.Mvc;
using Turner.Titles.Service;

namespace Turner.Titles.Web.Controllers
{
    public class TitleController : Controller
    {
        private readonly ITitleService _titleService;
        public TitleController()
        {
            _titleService = new TitleService();
        }

        // GET: Title
        public ActionResult List()
        {
            
            return View();
        }

        [HttpGet]
        public JsonResult Search(string query)
        {
            var titles = _titleService.SearchTitleByName(Server.UrlDecode(query));
            return Json(titles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int? id)
        {
            if(!id.HasValue)
                return RedirectToAction("List");

            var title = _titleService.GetTitleById(id.Value);
            if (title == null)
                return RedirectToAction("List");

            return View(title);
        }

        public ActionResult NotFound()
        {
            return View();
        }
        
    }
}