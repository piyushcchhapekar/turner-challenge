﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Turner.Titles.Web.Startup))]
namespace Turner.Titles.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
